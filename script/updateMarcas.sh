#! /bin/bash
pwd
if [ -d backup ];
then
echo "Sí, sí existe."
else
mkdir backup
fi

#Se indica la aseguradora a la cual se desea consultar las marcass

for var in general_seguros; do
  if [ -f script/$var.json ];
  then
    echo "Sí, sí existe "$var", movere archivos"
    mv script/$var.json script/backup/
  else
    echo "No existe "$var" no movere archivos"
  fi

  pet="$(curl https://dev.ws-general.com/v1/gs-car/brands)"
  if [[ $pet =~ ^\[ ]]; then
   echo "Es un array de marcas"
   if [[ ! $pet =~ ^\[\] ]]; then
     echo "Hay marcas en el array, se guardarán en el json"
     echo $pet >> script/$var.json
   else
     echo "El Array está vacio, se traerán las marcas del backup"
     cp script/backup/$var.json script/$var.json
   fi
  else
   echo "No, no es un array, se traerán las marcas del backup"
   cp script/backup/$var.json script/$var.json
  fi
done
