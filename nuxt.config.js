module.exports = {
  target:'static',
  /*
   ** Headers of the page
   */
  head: {
    title: "generalseguros.com.mx",
    htmlAttrs: { lang: "es-MX" },
    meta: [
      { charset: "utf-8" },
      { hid: "robots", name: "robots", content: "index, follow" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "General" },
    ],
    link: [{ rel: "icon", type: "image/png", href: "/favicon.png" }],
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3bd600" },
  /*
   ** Build configuration
   */
  build: {
    //analyze:true,

    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          // loader: "eslint-loader",
          exclude: /(node_modules)/,
        });
      }
    },
  },
  // include css
  css: ["static/css/bootstrap.min.css", "static/css/styles.css"],
  plugins: [
    { src: "~/plugins/apm-rum.js", ssr: false },
    { src: "~/plugins/filters.js", ssr: false },
  ],
  modules: 
    ['@nuxtjs/axios'], 
    // gtm:{ id: "GTM-T62QHCD" },
  env: {
    Environment:'DEVELOP',
    tokenData: '2/SfiAuJmICCW2WurfZsVbTvNsjLtrWdQ3gsHLqLSjE=',
    catalogo: "https://dev.ws-general.com",
    sitio: "https://p.generalseguros.mx",
    coreBranding: "https://dev.core-brandingservice.com",
    motorCobro: "https://p.generaldeseguros-comprasegura.com/",
    urlValidaciones: 'https://core-blacklist-service.com/rest',//PRODUCCIÓN
    urlGetConfiguracionCRM: 'https://www.mark-43.net/mark43-service/v1',//PRODUCCIÓN
    promoCore: "https://dev.core-persistance-service.com",
    urlTactos: 'https://ws-camiones.com',//https://ws-camiones.com/v1/camiones/marcas,
    hubspot:"https://core-hubspot-dev.mark-43.net/deals/landing",
  },
  render: {
    http2: { push: true },
    resourceHints: true,
    compressor: { threshold: 9 },
  },
};
