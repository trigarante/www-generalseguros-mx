import Vuex from 'vuex';
import getTokenService from '../plugins/getToken';

const tokenData = process.env.tokenData

const createStore = () => {
  return new Vuex.Store({
    state: {
      idEndPoint: '',
			status: '',
			message: '',
      cotizacionAli: "",
      cargandocotizacion: false,
      msj: false,
      msjEje: false,
      sin_token: "",
      tiempo_minimo: 1.3, //Tiempo minimo para validación de token
      config: {
        time: 5000,
        catalogos: false,
        tipoVehiculo: "AUTO", //AUTO|MOTO|TRACTO|LANCHA|BICI
        useBrandV2: false,
        habilitarBtnEmision: false,
        habilitarBtnInteraccion: false,
        btnEmisionDisabled:false,
        ipCliente: "",
        aseguradora: "",
        cotizacion: true,
        emision: true,
        interaccion: true,
        descuento: 0,
        telefonoAS: "88902327",
        grupoCallback: "",
        from: "",
        idPagina: 0,
        img: "aba.svg",
        alert: true,
        asgNombre: "GENERALDESEGUROS",
        promo: "Obtén",
        desc: "Increíbles descuento ó",
        msi: "3 y 6",
        dominio: "generalseguros.com.mx",
        dominioCorreo: "GENERALDESEGUROS",
        urlGracias: "",
        accessToken: "",
        idCotizacionNewCore: "",
        textoSMS:
          "Felicidades: Ya tenemos tu cotización en breve un ejecutivo te contactará.",
        idSubRamo: "",
        dataProspecto: [],
        dataCotizacionAli: [],
        id_cotizacion_a: "0",
        desc:'',
        msi:'',
        promoLabel:'',
        promoImg: '',
        promoSpecial:false,
        extraMsg: ''
      },
      ejecutivo: {
        nombre: "",
        correo: "",
        id: 0,
      },
      formData: {
        idHubspot:'',
        aseguradora: 'GENERALDESEGUROS',
        marca: '',
        modelo: '',
        descripcion: '',
        detalle: '',
        clave: '',
        cp: '',
        nombre: '',
        telefono: '',
        gclid_field: '',
        correo: '',
        edad: '',
        fechaNacimiento: '',
        genero: '',
        emailValid: '',
        telefonoValid: '',
        codigoPostalValid: '',
        urlOrigen: ''
      },
      solicitud: {},
      cotizacion: {},
      cotizacionesAmplia: [],
      cotizacionesLimitada: [],
    },
    actions: {
      getToken(state) {
        return new Promise((resolve, reject) => {
          getTokenService.search(tokenData).then(
            (resp) => {
              if (typeof resp.data != "undefined") {
                this.state.config.accessToken = resp.data.accessToken;
              } else {
                this.state.config.accessToken = resp.accessToken;
              }
              localStorage.setItem("authToken", this.state.config.accessToken);
              resolve(resp);
            },
            (error) => {
              reject(error);
            }
          );
        });
      },
    },
    mutations: {
      validarTokenCore: function(state) {
        try {
          if (process.browser) {
            if (
              localStorage.getItem("authToken") === null ||
              localStorage.getItem("authToken") === "undefined"
            ) {
              state.sin_token = true;
            } else {
              state.config.accessToken = localStorage.getItem("authToken");
              var tokenSplit = state.config.accessToken.split(".");
              var decodeBytes = atob(tokenSplit[1]);
              var parsead = JSON.parse(decodeBytes);
              var fechaUnix = parsead.exp;
              /*
               * Fecha formateada de unix a fecha normal
               * */
              var expiracion = new Date(fechaUnix * 1000);
              var hoy = new Date(); //fecha actual
              /*
               * Se obtiene el tiempo transcurrido en milisegundos
               * */
              var tiempoTranscurrido = expiracion - hoy;
              /*
               * Se obtienen las horas de diferencia a partir de la conversión de los
               * milisegundos a horas.
               * */
              var horasDiferencia =
                Math.round(
                  (tiempoTranscurrido / 3600000 + Number.EPSILON) * 100
                ) / 100;

              if (hoy > expiracion || horasDiferencia < state.tiempo_minimo) {
                state.sin_token = "expirado";
              }
            }
          }
        } catch (error2) {
          console.log(error2);
        }
      },

    },
  });
};
export default createStore;
