import axios from 'axios'

let urlConsumo = process.env.catalogo + '/v1/gs-car'


class Catalogos {
  marcas() {
    return axios({
      method: "get",
      url: urlConsumo + `/brands`,
      // headers: { Authorization: `Bearer ${accessToken}` },
      // params:{marca}
    })
  }
  modelos(marca, accessToken) {
    return axios({
      method: "get",
      url: urlConsumo + `/years?brand=${marca}`,
      // headers: { Authorization: `Bearer ${accessToken}` },
      // params:{marca}
    })
  }
  submarcas(marca, modelo, accessToken) {
    return axios({
      method: "get",
      url: urlConsumo + `/models?brand=${marca}&year=${modelo}`,
      // headers: { Authorization: `Bearer ${accessToken}` },
      // params:{marca,modelo}
    })
  }
  descripciones(marca, modelo, submarca, accessToken) {
    return axios({
      method: "get",
      url: urlConsumo + `/variants?brand=${marca}&year=${modelo}&model=${submarca}`,
      // headers: { Authorization: `Bearer ${accessToken}` },
      // params:{marca,modelo,submarca}
    })
  }
}

export default Catalogos;
